import java.awt.*;
import javax.swing.*;


public class Client extends JFrame {
	
	JTextArea messagesArea = new JTextArea(300, 300);

	public Client() {
		super("Chat App");
		setVisible(true);
		setSize(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(600,200,600,400);
		Container mainPanel = getContentPane();
		mainPanel.add(messagesArea, BorderLayout.CENTER);
		mainPanel.add(new JTextField(), BorderLayout.SOUTH);
		
	}

	public static void main(String[] args) {
		new Client();

	}

}
