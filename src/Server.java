import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {

	private ServerSocket serverSocket;
	private Socket socket;
	private BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	private PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	private String message;
	
	private void listen(){
		while(true){
			try {
				message = in.readLine();
				out.println(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Server(int portNumber) throws IOException{
		try{
			/*
			 * Add ServerSocket
			 * Link the Socket to the ServerSocket
			 * Display message to console
			 */
			serverSocket = new ServerSocket(portNumber);
			socket = serverSocket.accept();
			System.out.println("Currently listening at PORT "+portNumber);
		} catch(IOException e){
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		new Server(4444).listen();
	}

}
